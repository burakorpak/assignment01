#!/usr/bin/env bash

echo "Please give your name"
echo "*********************"
read name;

if [ ! -d "/home/$name" ]; then
	echo "Requested $name user home directory doesn't exist."
    		exit 1
else 
	echo "Hi $name! Your name is correct!"
fi
get_process=$(ps acxho user,pid,%cpu,cmd | grep "$name")
get_pid=$get_process | grep pid
get_cpu_perc=$get_process | grep %cpu
get_cpu_cmd=$get_process | grep cmd

function make_colorful {
	echo -e "\033["$1"m"
}


printf "|%-13sUSER%-13s|%-13sPID%-13s|%-13sCPU%-13s|%-20sCMD%-20s|\n "
for i in "$get_process"
do
printf "%-30s|%-29s|%-29s|%-43s|\n" $get_process $get_pid $get_cpu_perc $get_cpu_cmd 
done


