#!/usr/bin/env bash
##############################################################
#Request username confirmation, through user' s home directory
#if the directory exist and if you lucky you will see rainbow
#if it is not, it will be terminated.
##############################################################
echo "Please give your name"
echo "*********************"
read name;

if [ ! -d "/home/$name" ]; then
	echo "Requested $name user home directory doesn't exist."
    		exit 1
else 
	echo "Hi $name! Your name is correct!"
fi


colorized=30
ps aux --width=100 | grep $name | while read line; do
	echo -en "\e[38;5;${colorized}m${line}\e[0m" ; 
	echo 
	let colorized+=1
done

